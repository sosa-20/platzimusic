import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import ArtistBox from './ArtistBox';




export default class ArtistDetailView extends Component {
 
  render() {
    artist= this.props.artist
    return (
      <View style={styles.container}>
        <ArtistBox artist={artist} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'lightgray',
  },
});