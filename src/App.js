import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ArtistBox from './ArtistBox';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import HomeView from './HomeView';
import ArtistDetailView from './ArtistDetailView'
import {Router, Stack, Scene, Platform} from 'react-native-router-flux'
import LoginView from './LoginView';


class PlatziMusic extends Component {
  
render(){
  return (
  <Router>
    <Stack key="root">
    <Scene key="login" component={LoginView} hideNavBar/>
      <Scene key="home" component={HomeView} hideNavBar/>
      <Scene key="artistDetail" component={ArtistDetailView} title="Detalle Artista"/>
    </Stack>
  </Router>
)
}}
export default PlatziMusic
