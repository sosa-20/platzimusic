import React, {Component} from 'react';
import {StyleSheet, View,Text} from 'react-native';
import FBSDK, { LoginButton, AccessToken } from 'react-native-fbsdk';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Actions } from 'react-native-router-flux';


export default class LoginView extends Component {
  handleLoginFinished = (error,result) => {
        if (error) {
          console.log("login has error: " + result.error);
        } else if (result.isCancelled) {
          console.log("login is cancelled.");
        } else {
          AccessToken.getCurrentAccessToken().then(()=>{
            console.log("redirigiendo");
              Actions.home()
          }
            
          )
        }
      }
  render() {
   
    return (
      <View style={styles.container}>
        <Text style = {styles.welcome}>Bienvenido a PlatziMusic</Text>
        <LoginButton
        readPermissions={['public_profile','email']}
          onLoginFinished={this.handleLoginFinished
          }
          onLogoutFinished={() => console.log("logout.")}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'lightgray',
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcome: {
      fontSize: 24,
      fontWeight: '600',
      marginBottom: 20,
  }
});
