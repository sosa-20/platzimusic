/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {FlatList, TouchableOpacity } from 'react-native';

import ArtistBox from './ArtistBox';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Actions } from 'react-native-router-flux';

export default class ArtistList extends Component {
  handlePress(artist){
    Actions.artistDetail({artist: artist})
  }
  render() {
    //console.log(this.props.artist);
    return (
      <FlatList
        data={this.props.artist}
        renderItem={({item}) =>{ 
        return (
           <TouchableOpacity onPress={() => this.handlePress(item)}>
        <ArtistBox artist={item}></ArtistBox>
        </TouchableOpacity>)}}
        
      />
    );
  }
}
